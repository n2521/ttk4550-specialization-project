# Plan for the project

Any great project starts with a great plan. I will divide the project into the following phases:

1. Literature research
2. Hardware acquisition
3. Software development
4. Data acquisition
5. Report polishing

I have until week 51 to complete the project and report. As of writing it is week 36 which gives me 15 weeks to work with.
I will initially allocate the time acording to this table

| Milestone                              | Deadline | Total |
|----------------------------------------|----------|-------|
| Research                               |   W39    |   4   |
| Specification finished                 |   W40    |   2   |
| Hardware should be ordered/built       |   W49    |   2   |
| Software should be developed           |   W47    |   5   |
| Data acquisition should be concluded   |   W49    |   3   |
| Report should be finished and polished |   W51    |   3   |


## Research phase

During the research phase I will try to find other reports or papers that have tried or done what I want to measure.
In this phase I will also do research on different mesh technologies and figure out which of the technologies I want
to use in the project going forward.

In short terms this will be the structure of this phase:

1. Literature search
    Find related literature or similar research
2. Create overview of different mesh network protocols
    Set up a list of their strengths and weaknesses, then
    find a set of these that will be viable to analyze
3. Create test setups and test scenarios.
    Find out what kind of data that makes sense to measure and
    compare against other protocols

## Hardware acquisition phase

A few weeks is required to actually get my hands on the required equipment that I need and get familiar with them.

## Software development phase

In the development phase I create the required software that is needed to actually measure meaningful data from the different mesh technologies.

It will loosely be structured in the following way:

1. SDK diving
    It is important that I do not get caught trying to reinvent the wheel. Trying to find
    appropriate SDK examples is very important

2. Stiching all found examples together.
3. Debugging issues.

## Data acquisition phase

This phase will most likely meld a bit in to the software development phase as I will most likely find a bunch of issues that needs to be fixed
to be able to continue. It is important to focus on the possible error sources that I have in my setup which will be very interesting to discuss in the report.
In this phase I will also spend a lot of time on analyzing measurements and data and try to tie them up to my chosen performance metrics.

## Report polishing phase

I will write on the report continuously during the entire project, but a few weeks at the end is required to be able to hand in a good looking
and easy to read report.
It is also possible to cut this phase a bit shorter if I am desperate for some extra time for the project.

