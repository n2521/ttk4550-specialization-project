# Notes and conversations about the specialization project

## Meeting with Geir

Overall we talked about the idea of finding a method to optimize power usage in an IoT network
that communicates via mesh by gathering all data on designated link nodes that then in
turn send the data to a LTE network. An important thing to keep in mind is that the 
specialization project and the masters thesis should be different enough that they
will get a good grade individually, while they are related enough so that I can
work with what was discovered in the specialization project.

### Specialization project

Attempt to look at the power consumption/cost of an LTE uplink, GPS fixing and different mesh networks.
Try to examine different topologies to get a broad understanding of the cost.
Things to figure out

* How often do you communicate over mesh?
* What dictates packet sizes?
* How does different technologies and protocols compare?

### Masters degree

'In a mesh network, is it possible to create a scheme that can promote designated uplink nodes
for transmitting data over a more costly network to save power?'

Things to solve:

* How can you connect or disconnect nodes in a network?
* Is it possible to merge networks?

### General tips and requirements for the work

Geir wants the following when starting out:

1. A project working title
2. Do a litterature search
3. Specification of the task
    * What are the focus areas?
    * Where will I focus my time?
4. Work in a design -> implementation -> testing workflow
    * Set deadlines to not get stuck
5. Discussion. Have I fullfilled the spec?
6. Proposal for further work.
7. Conclusion

At the start he wants half a page of what I want to do and a time schedule for the work.

*Focus on the most important first!*

Throughout the projects Geir wants a weekly meeting and a weekly progress report to make
sure time is not wasted and that sufficient progress is being made.

