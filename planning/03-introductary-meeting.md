# Introductionary meeting with specialization project

The specialization project is a "practice" for the Masters Thesis

Weekly once a week.

Discuss with my supervisor what my project will be, very concretely.
Write a page or so with a description of the task.

A detailed schedule should be created that describes the work required until December.

## Report template

First of all, in the beginning of the text that describes the different tasks of the report. This is what the examinator will use to judge whether or not the report fulfills the task.

### Preface

If you borrow/collaborate with someone it should be included in the preface

### Summary

### Contribution

Can be omitted if no revolusionary findings or ideas are created.

### Literary study

Separated into different findings and then summarize what has been found.
This section should help us make sure that no solution to our problem is not already thouroughly studied or implemented.

### Theory

Only if necessary, i.e. the theory is complex.


### Proposed solutions / design

Start with a functional specification. Will be great to refer to later in the task.

### Implementation

Concrete:
* Which compiler is used?
* What language is used?

Should be thorough enough to be able to reproduce the results.

### Results

No discussion!

### Discussion

How to we interprate the results?

### Conclusion

Duh.

### Further work

### References

### Appendecies


