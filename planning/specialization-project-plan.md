# Project plan, goals and info

This will be the general plan for the specialization project for fall 2021.

## Working title

> Energy cost of data uplink, GPS and mesh network communication in IoT networks.

## What do I want to do?

Try to do a analysis of the energy cost of different data transfer protocols commonly used in IoT. The goal is to try to create a nice overview of the energy cost of transmitting data internally in an IoT cluster and externally via an uplink like 4G or through a gateway via bluetooth (or similar close range data transmission protocol).
Since a lot of different technologies with different strengths and weaknesses exists a broad analysis of their energy usage would be interesting.
The inclusion of the external uplink via 4G or a gateway is to be able to perform a comparison between their costs. This in turn can be used as a reference to design a strategy of data transmission to potentially save power by utilizing the fact that there might be many nodes that have uplink capabilities. The difference in transmission cost of the internal and external communication can be significant enough that it may be more efficient to promote uplink nodes that does all external communication than for every node to do their own uplink communication.

## Project description

The following assignemnt text is the basis for this report:


*I dagens IoT-marked er energisparing veldig viktig. I denne oppgaven vil det, i grove trekk, bli 
gjennomført en analyse av energiforbruket til forskjellige mesh protokoller og 
dataoppkoblinger som LTE-M eller NB-IoT. I analysen vil fokuset ligge på å sammenlikne 
node-node og node-sky-kommunikasjon for å se om det finnes markante forskjeller i 
energibruk. Dette kan videre bli sett på i en kontekst der antallet noder er relativt stort og se 
på hvordan energiforbruket endrer seg med antallet noder.
Oppgaven vil starte med å finne et subsett med relevante mesh-protokoller som kan være 
interessante å ha med i analysen.
Etter å ha valgt ut noen mesh-teknologier må det finnes en måte å programmere mesh-
nodene til å sende data mellom hverandre som gjør at det er mulig måle energiforbruket opp 
mot et eller annet måltall, f.eks. energiforbruk/bit eller noe liknende. Utfordringer her vil være 
hvordan man skal måle energiforbruket til hver av enhetene og hvordan de sammenliknes. 
Energiforbruket til de forskjellige teknologiene kan være avhengig deres særegne måte å 
sende data på og om teknologien gjør det mulig å faktisk koble strømforbruket opp mot et 
eller annet brukbart måltall. Den samme metodikken vil brukes på LTE-M og/eller NB-IoT for å
få et bilde om hvor mye en node-sky-kommunikasjon koster. Det er viktig at fokuset i den 
siste biten, der man ser på node-sky-kommunikasjon, er på at tallene som anskaffes er på et 
format som gjør det mulig å sammenlikne med node-node-kommunikasjonen på en 
meningsfull måte.
I siste delen av oppgaven så vil det bli foretatt en analyse av de resultatene som har blitt 
anskaffet i tidligere deler. I analysen vil som tidligere nevnt fokuset være å sammenlikne 
node-sky- og node-node-kommunikasjon i IoT-nettverk og se om de to metodene gir noen 
store forskjeller i energiforbruk. Videre kan det være interessant å se om det mulig å trekke 
noen slutninger om systemer der antallet noder øker og evenuelt hvordan forskjellige 
topologier vil påvirke energiforbruket i slike systemer.
Videre arbeid kan også innebære å gjøre en grundigere analyse av hvordan forskjellige typer 
kommunikasjon påvirker systemets energiforbruk. For eksempel hvordan vil en 
kommunikasjonskanal som krever en etableringsfase påvirke systemet satt opp mot en 
kommunikasjonskanal som tillater sporadisk sending.*


## Progress plan

In the [progress plan](./05-project-planning.md) I discuss the different phases that I will go through in this project and how much time I will
allocate to each of them.

