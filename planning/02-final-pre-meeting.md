# Meeting 2021-05-10

With Geir and Eivind to iron out any last details about the project.

Håkon has approved dev kit purchases and any cost below 10k NOK is a "no-brainer".

This is the current working title:

> Energy cost of data uplink, GPS and mesh network communication in IoT networks.

And the rest of the plan can be found in [this document](specialization-project-plan.md).

## Notes

Kan bli alt for generell om jeg ikke har en litt klar case som jeg skal se på.
Prøv å finn et reelt case som kan gjøre det litt mer konsentrert om en spesifikk problematikk.

## What to do now

In the introduction of my report I need to do limitations as per the use case that I want to use.

Where do I register the project?

    On the TTK4550 website project website.

When do I begin?

    End of August

Communication between Norbit and Geir? Is it all through me?

    Yes, it won't be a problem.

Can I use Redmine for project planning and SCRUM and then reference that in the project status reports?

    Yeah, sure.

Find use cases before starting the project.

    Norbit might be able to find some for me.

