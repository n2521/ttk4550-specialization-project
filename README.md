# TTK4550 - Specialization project

Documentation, code and report for my specialization project.

## Planning phase

All the planning stuff can be found in the [project plan](planning/specialization-project-plan.md).
Here all meetings around the project planning, the project progress plan and all such things will be found.

## Progress reports

My supervisor wants a progress report for each week to make sure that sufficient progress is being made. This
will be located in the `progress-reports` folder.

## Report

In the `report` folder you can find the report source code for this project.
It is written using LuaLaTex and is compiled with a simple `Makefile`

It requires a few packages to work out of the box on a Debian based system simply run:

```
sudo apt install texlive-full python3-pygments
```
