# Dataset explanation

## NeoCortec datasets

### idle_mid_node.csv

This is a sample of an idle NeoCortec module with two neighbours. In this sample it should only transmit beacon frames and turn on radio at scheduled intervals.

### send_ack_package.csv

This sample is a sample where we transmit an ack package from one NeoCortec to another. So here we should identify beacon packages as well as one transmission, if that is even noticeable.

### max_payload_relay.csv

This sample is from the mid node of a straight graph mesh networks that continuously relays max length payloads between end nodes

### constant_tx_neomesh.csv

This sample is from a mid node constantly transmitting a 2 byte message to another node

## LTE CoAP datasets

For these datasets a CoAP message is sent with a known size.

### coap2.csv

This dataset is a capture of 2 CoAP transmissions from the nRF9160. In this sample CONFIG_SYS_POWER_MANAGEMENT was not enabled, which may lead to slightly higher average current consumption because the Zephyr kernel wakes up each tick, even if no work is required.
We also leave the 4G modem on between transmissions with no power saving features.
The size of this CoAP packet is 

### coap3.csv

In this data set the CONFIG_SYS_POWER_MANAGEMENT is enabled, which makes the Zephyr kernel sleep more efficiently. We have also increased the time between the CoAP transmissions to 30 seconds witch means we only capture a single transmission.
The modem is left on between transmissions with no power saving features enabled.

### lte_coap_cold_start_w_uart.csv

This is a set of data from a cold start LTE connect and then one CoAP transmission. In this dataset the UART logging is included, interfering with measurements

### lte_coap_cold_start.csv

This is a set of data from a cold start LTE connect and then one CoAP transmission. The sample is started when the 4G modem attempts to connect to the LTE-M network.

### psm_coap_w_uart.csv

A dataset where the nRF9160 enters its alloted timeslot when using PSM with UART enabled.

### ./coap_from_psm_60s_active_time.csv

A CoAP transmission of 11 bytes(no user data) sent from PSM state with 60 seconds active time

### ./coap_from_psm_60s_active_time_21B_pl.csv

A CoAP transmission of 32 bytes(21 bytes payload) sent from PSM state with 60 seconds active time

### ./coap_from_psm_60s_active_time_210B_pl.csv

A CoAP transmission of 221 bytes(210 bytes payload) sent from PSM state with 60 seconds active time

## OpenThread CoAP datasets

### fed_rx.csv

A sample of a FED router that receives a light request

### ot_tx_sed.csv

A sample of a SED that sends a light request to a FED router

## Config files

### config_1.toml

This is the first configuration to be tested which is pretty much the highest power usage configuration in NeoMesh

## GPS

### ./pure_gps_search.csv

It is a pure GPS search, no events happening.

### ./gps_from_active_time.csv

The modem goes from LTE mode during its active time into GPS search mode.
