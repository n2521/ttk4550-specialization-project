# Data set info

## single_idle_with_cts_pin.csv

Single node with markers when CTS goes high

## full_relay_1s_beacon_1s_sch_tx_middle_node

The measured node is the middle node, it is constantly receiving messages to transmit to another
node. Scheduled TX is 1s and the Beacon Rate is 1 second.


## middle_node_idle

Same as above only no transmission.


## lte_cold_start

Just a cold start LTE on the nRF9160 into PSM mode.


