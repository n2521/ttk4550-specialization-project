
CONFIG=$1
NODES="neocortec-2.local neocortec-3.local"
CMD="sapi-cli -D /dev/ttyUSB1 config --password Lvl10 import"


for node in $NODES; do
    echo "uploading on $node"
    ssh pi@$node "\$HOME/.cargo/bin/$CMD $CONFIG; echo '$node is done'" &
done

$CMD $CONFIG

wait

echo "All configs uploaded..."
