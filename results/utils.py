import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.signal import find_peaks


def import_dlog(filename: str):
    """ Helper function that extracts a dataframe from a CSV file """
    header_lines = 6
    header = ""
    reg = re.compile(r"(\w*) avg 1")


    with open(filename, "r") as fh:
        header = [fh.readline() for _ in range(header_lines)]
        # Extract the sample time from the header
        sample_interval = float(next(filter(lambda s: "Sample interval" in s, header)).replace("\"", "").split(":")[1].strip())
        trigger_sample = int(next(filter(lambda s: "Trigger sample" in s, header)).replace("\"", "").split(":")[1].strip())

        df: pd.DataFrame = pd.read_csv(fh)

    # Rename columns to remove unimportant index
    df.rename(columns=lambda c: re.sub(reg, r"\1", c), inplace=True)
    
    # Create new column with the time since trigger
    # set it as the new index and then drop the sample
    # column. It is not needed
    df["Time"] = df["Sample"] * sample_interval
    df["Time"] = df["Time"] - df["Time"][0]
    df.set_index("Time", inplace=True)
    df.drop(columns=["Sample"], inplace=True)
    
    # If the measurement has voltage and current measurements
    # we calculate the watt as well, since it is nice to have

    if "Volt" in df.columns and "Curr" in df.columns:
        df["Watt"] = df["Volt"] * df["Curr"]
    return df

def locate_transmissions(df, height, minimal_transmission_delay):
    curr = df["Curr"].values
    sample_interval = df.index[1] - df.index[0]
    distance = minimal_transmission_delay / sample_interval
    peaks, _ = find_peaks(curr, height=height, distance=int(distance))
    peaks = df.iloc[peaks].index

    return peaks

def save_plot(filename):
    plt.savefig(filename, bbox_inches='tight', dpi=600)

def import_ppk(filename: str, digital_io=None):
    df = pd.read_csv(filename, index_col=0)
    if "D0-D7" in df.columns:
        df["D0-D7"] = df["D0-D7"].map(lambda x: 1000 if x == 0 else 0)
        df = df.rename(columns={"D0-D7": "CTS"})
    elif "D0" in df.columns:
        digital_io = digital_io or []
        cols = [f"D{i}" for i in range(8) if i not in digital_io]
        df = df.drop(columns=cols)
        
    
    start = df.index[0]
    df.index = df.index.map(lambda x: x - start)
    return df
