\section{Introduction}

This section will describe the background, motivation and the scope of this thesis. In the end of the section, the structure of the thesis will be presented.

\subsection{Background}

When a sheep farmer releases their sheep into the wild during grazing season, the sheep usually stray far from where they were originally released.
As the end of grazing season comes, the farmer needs to collect all their sheep and bring them back to the farm. The process of gathering the sheep is a large ordeal, as the sheep can be spread over an extremely large area, and often a team of people is employed to make this as efficient as possible.

A seasoned farmer may have a good idea where to find their sheep and they will most likely have dogs to help them, but gathering the sheep is still a challenge. Especially if no tracking devices are used, since there is no way to pinpoint their location.
Employing a large team of people will be helpful, but it is both expensive and the process still time consuming.

To streamline the sheep gathering process one can attach a tracker to each sheep in the herd and use a mobile device to track their location when collecting the sheep.
This scenario will be the baseline of this thesis.

A few immediate design requirements are apparent: the tracking unit will be required to have a battery life that is longer than at least the average grazing season. The tracking unit will need some way of communicating with a cloud solution and tracking unit will have to be light enough so that the sheep can carry them, without being tired out.

The issue with such a tracker is that communicating via f.ex. a 4G radio or searching for GNSS position is quite expensive in terms of energy usage and having a full herd of sheep transmitting 4G or searching for GNSS fixes will require a lot of energy in total.

\subsection{Motivation} 

In the industry today sheep tracking is somewhat used, but to make sure the tracker has enough power it requires a large battery. The battery is heavy, so it will only be able to be attached to a sheep, not a lamb \cite{smartbjelle_home}, \cite{findmy_home}, \cite{telespor_home}.

This is an issue because a lamb can get away from it's mother, f.ex. in a predator attack, and thus we will lose track of the lamb since no tracker is attached to it. If a sheep is lost or dies the lambs are mostly considered lost as well because the lambs cannot survive without their mother.
Farmers often just refer to a mother and its two lambs as a \textit{set}, since the lambs are so dependent on their mother.
Losing a few sheep or lambs each season is normal, but with a tracker, that is small enough to be attached to a lamb, we could potentially reduce these losses.

Now lets work under the assumption that a set of sheep, i.e. one mother and two lambs, stay close to each other during their stay at the pastures. If we deploy an ultra low power, short range mesh network to enable communication between nodes(trackers are considered nodes when in a mesh network), in addition to the existing data uplink to the cloud, we may be able to achieve very low energy usage.
This allows us to reduce the size of a trackers included battery, so that we may attach them to lambs as well.

This is the main motivation of this thesis; is it possible to utilize a low power mesh network, in conjunction with a gateway protocol and positioning system, to reduce energy usage of a sheep tracker?

The sheep tracking scenario is a nice practical example to help set the scope when exploring this sort of energy conservation. In \cref{sec:discussion} a few points about generalization will be presented.

\subsection{Scope}

% +++++++++++++++++++++++++++++++++++++++
% Conversation with Jenny:
% ========================

% The farmer count sheep in "sets", one sheep and two lambs. If a sheep or lambs are lost the set is considered lost, which means that if a sheep dies the lambs are also cosidered lost.
% A farmer will probably be interested in tracking both sheep and lambs to reduce losses in the long turn.
% Mounting a tracker to a lambs might be difficult. During grazing season the lambs start out at about 10-15kgs and will probably gain about 10-15 kgs during a season, which means
% that they grow quite a bit and it might be problematic to equip them with a collar as they might grow so much that the collar will grow tight. I believe that this is a solvable problem and should not affect my task too much.
% Using mesh clusters on sheep will synergize well with the sheeps natural behaviour. Lambs almost never stray far from their mother and they naturally tend to stay in packs.
% If this would turn in to a product at later stages prices are paramount as a farmer may have 400 sheep and 800 lambs or so.
% ++++++++++++++++++++++++++++++++++++++++

The world of IoT is very complex and there are heaps of considerations and design decisions to make. To be able to finish the work on this thesis a few limitations to the scope must be made.

A simpler subset of cases in the scenario must be chosen to focus on, as developing and testing all cases in the sheep tracking scenario is not feasible in a single thesis.
First let us assume we have a single set of sheep, that are each fitted with the proposed tracker with mesh, GNSS and gateway functionality.
And assume that this set sticks together, like they will under normal conditions. How much does it cost for each sheep to collect GNSS data and send it to the cloud, compared to how much it costs if one tracker collects GNSS for the set, and one dedicated gateway receives this information, via mesh, and then transmits it to the cloud?
How much energy does it cost to keep the mesh network active? What happens to the integrity of the mesh network if a lamb wanders off for a moment? How does this energy cost vary between different mesh protocol?

If this case yields reasonably good results, we could potentially increase the amount of energy saved. This may be done by using a larger amount of nodes, if additional sets of sheep are within mesh network range.
With a larger amount of nodes, an increase in network traffic will follow, to maintain the mesh network and routes. More sets of sheep will also mean that our mesh network may be constantly changing, which will be much more complex to model. This will be a possible area for more research.

To further specify the scope of the thesis we make a few assumptions that will help to reduce the amount of time spent on technicalities and deployment specific issues.

First of all, we assume that the trackers' gateways always have coverage.
The effects of a loss of connection to the cloud could be mitigated by storing data in a non-volatile storage, but we will not delve any deeper into this issue.

We assume that a mounting solution is available to attach trackers to the sheep and lambs. A lamb will grow quite a lot during grazing season \cite{utmarksbeite_sau} and it might not be trivial to find a way to attach the tracker to f.ex. their necks, but this is definitely outside the scope of this thesis.

We assume that security and provisioning of nodes, i.e. handing out network keys or certificates to nodes, is not an issue.
This will be a highly deployment specific problem, it will depend on many choices of technology, but in the thesis we will assume that every node is pre-provisioned and that all communication is secure.


\subsection{Thesis structure}

In \cref{sec:research}, \nameref{sec:research}, an overview of existing solutions and literature about the subject will be presented.
Then, in \cref{sec:theory}, a brief introduction to theory that will be used in the rest of the thesis.

Following, in \cref{sec:design,sec:test_spec}, \nameref{sec:design} and \nameref{sec:test_spec}, a proposal of a design for a sheep tracker together with a specification of how to test it.

In \cref{sec:impl}, \nameref{sec:impl}, the proposed design will be implemented and tested.
\Cref{sec:results}, \nameref{sec:results}, will present our findings. These findings will be discussed in \cref{sec:discussion} before a conclusion is presented in \cref{sec:conclusion}.
Lastly a brief listing of further work is presented in \cref{sec:further_work}.

