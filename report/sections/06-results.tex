\section{Results}\label{sec:results}

In this section the results from the data acquisition will be presented and error sources will be discussed.

\subsection{NeoMesh}

Since the NeoMesh documentation, at the time of writing, does not contain any detailed power profile some analysis was required.
As described above we expect to see three kinds of radio events from the NeoMesh nodes. A beacon, a scheduled transmit and N scheduled receives, one for each neighbour.
To identify the different events we first run a single node without any neighbours. This means that after the initial full beacon scans the node should only transmit beacons on a known interval and transmit on another known interval. To make it even easier the CTS interleave setting was set on the node, which means that the CTS line would only be available when a TX event happens.
So using the IO ports on the PPK a dataset with the current usage and the value of the CTS pin was acquired and it is quite trivial to then identify the two events.
The beacon and scheduled TX events are shown in \cref{fig:nm_beacon_and_tx}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/beacon_and_tx_events.png}
    \caption{A single NeoMesh beacon and scheduled transmit event}
    \label{fig:nm_beacon_and_tx}
\end{figure}

Now to identify the last signal needed to do a full analysis, the scheduled receive event. To identify this event a second node was introduced into the system and became the neighbour node of our original node. Then we repeated the process mentioned above except the third event was identified.
Then to end up with the topology that we want to run further tests on we included a third node, being neighbour only to our original node.

With this setup and the knowledge about the events we can now measure and identify all signals in this topology, presented in \cref{fig:nm_event_overview}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/nm_event_overview.png}
    \caption{An overview of all NeoMesh radio events}
    \label{fig:nm_event_overview}
\end{figure}

After reviewing over 75 different events the mean current during each event is given in \cref{tab:nm_energy_overview}.

After measuring idle energy consumption we move on to the measurements when sending data. One outer node is constantly transmitting a full sized payload through another outer node, this means that the middle node will receive, then relay this message. Using the same technique as above we find the CTS signal when the nodes are transmitting and we find a transmit event with data, shown in \cref{fig:max_len_transmit}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/singe_21_byte_tx.png}
    \caption{Transmitting a message with maximum payload size}
    \label{fig:max_len_transmit}
\end{figure}

Lastly we want to figure out the sleep current of the mesh node. A overview of the sleep current is shown in \cref{fig:nm_sleep_current}. To obtain these results we have filtered out any data points above $0.8 mA$ and then taken the mean of the remaining data. This means that we get a slightly higher mean that the exact sleep current, but with a data set of 120 seconds it should account for very little.
The data seems noisy because we get the start and end of the radio events included in our measurements.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/sleep_current.png}
    \caption{NeoMesh sleep current, no radio events}
    \label{fig:nm_sleep_current}
\end{figure}

When events where identified a number of them were sampled to create a better average overview of the cost of each event. For the beacons, receive and transmit events 25 events were used to create an average.
For the full beacon scan only 7 events, but the variance of the data was so low that it should not matter.
A summary of these measurements are shown in \cref{tab:nm_energy_overview}.

\begin{table}[H]
    \centering
    \begin{tabular}{llll}
        \textbf{Event}              & \textbf{Duration}         & \textbf{Mean current} & \textbf{Charge usage} \\\hline
        Beacon                      & $5.162 ms$                & $21.622 mA$           & $111.60 \mu{}C$ \\
        Scheduled transmit(1dBm)    & $2.394 ms$                & $12.838 mA$           & $30.724 \mu{}C$ \\
        Scheduled transmit(-30dBm)  & $2.451 ms$                & $10.27 mA$            & $25.181 \mu{}C$ \\
        Scheduled receive           & $2.585 ms$                & $15.953 mA$           & $41.244 \mu{}C$ \\
        Full payload transmit       & $4.577 ms$                & $17.59 mA$            & $80.511 \mu{}C$ \\
        Full beacon scan            & $1000 ms$ ($T_{beacon}$)  & $24.521 mA$           & $24.664 mC$ \\
        Sleeping                    & N/A                       & $0.895 uA $           & $t_{sleep} \cdot 0.06048\mu{}C$ \\
    \end{tabular}
    \caption{Overview of average energy usage of NeoMesh radio events}
    \label{tab:nm_energy_overview}
\end{table}

Based on the results in \cref{tab:nm_energy_overview} we get the following model for average current usage, based on $N_{neighbour}$, $T_{beacon}$, $T_{full scan}$ and $T_{tx}$:

First we make find the percentage of time spent on each event(\cref{eq:nm_event_percentages}).

\begin{subequations}
\begin{align}
    p_{beacon} &= \frac{t_{beacon}}{T_{beacon}} = \frac{5.35\cdot10^{-3}}{T_{beacon}}\\
    p_{tx} &= \frac{t_{tx}}{T_{tx}} = \frac{2.44\cdot 10^{-3}}{T_{tx}}\\
    p_{rx} &= N_{neighbour} \cdot \frac{t_{tx}}{T_{tx}} = N_{neighbour} \cdot \frac{2.61\cdot10^{-3}}{T_{tx}}\\
    p_{full scan} &= \frac{T_{beacon}}{T_{beacon} T_{full scan}} = \frac{1}{T_{full scan}} \\
    p_{sleep} &= 1 - (p_{beacon} + p_{tx} + p_{rx} + p_{full scan}) \\
\end{align}
\label{eq:nm_event_percentages}
\end{subequations}

And we now calculate the mean current of each event over time:

\begin{subequations}
\begin{align}
    i_{beacon} &= \bar{i}_{beacon} \cdot p_{beacon} = 21.622\cdot p_{beacon}\\
    i_{tx} &= \bar{i}_{tx} \cdot p_{tx} = 12.72 \cdot p_{tx}\\
    i_{rx} &= \bar{i}_{rx} \cdot p_{rx}  = 15.71 \cdot p_{rx}\\
    i_{full scan} &= \bar{i}_{fullscan} \cdot p_{full scan} = 14.521 \cdot p_{full scan} \\
    i_{sleep} &= \bar{i}_{sleep} \cdot p_{sleep} = 0.000895 \cdot p_{sleep}\\
\end{align}
\label{eq:nm_event_currents}
\end{subequations}


Then we can express the average idle current draw as in \cref{eq:nm_idle_current_model}.

\begin{equation}
    I_{avg} = i_{sleep} + i_{tx} + i_{rx} + i_{beacon} + i_{full scan} 
\label{eq:nm_idle_current_model}
\end{equation}

We can also make a simple model that includes transmitting packages based on the percentage of times we expect to transmit a package ($p_{payload}$).
We add \cref{eq:nm_payload_currents}: 

\begin{subequations}
    \label{eq:nm_payload_currents}
    \begin{align}
        p_{tx,payload} &= \frac{t_{tx,payload}}{T_{tx}} = \frac{4.577\cdot10^{-3}}{T_{tx}} \\
        i_{tx,payload} &= 17.59 \cdot p_{tx,payload} \\
        \tilde{i}_{tx}(p_{payload}) &= p_{payload} \cdot i_{tx,payload} + (1 - p_{payload}) \cdot i_{tx} 
    \end{align}
\end{subequations}

And modify \cref{eq:nm_idle_current_model} to end up with \cref{eq:nm_transmit_current_model}: 

\begin{equation}
    I_{avg,transmit}(p_{payload}) = i_{sleep} + \tilde{i}_{tx}(p_{payload}) + i_{rx} + i_{beacon} + i_{full scan} 
\label{eq:nm_transmit_current_model}
\end{equation}

It is important to note that this model is not exact. It does not take into account a few complex scenarios.

Because of the slight imperfect period the transmit or receive events may overlap a beacon transmission event, this is not accounted for by the model. It happens fairly rarely, so the effect will be minimal, which is why it has been neglected.
When nodes are in a mesh with more than a few neighbours nodes may also skip random beacon transmissions to avoid collisions, this is not accounted for.

In the proposed models the fact that $N_{neighbour}$ is assumed constant may also lead to errors in the model. The neighbour count may, especially in our example be changing dynamically and the nodes will have to increase or decrease the amount of receive events. For example if a set of sheep moves in and out of a complete mesh into a string like topology the neighbour count will switch between one and two neighbours.
A way to model for this would be to change the weighting of the receive event in our model to use the expected average neighbour count in stead of the static neighbour count.

To test out the viability of the model it was applied to three samples. Two samples with two neighbours, 1 second scheduled transmit and 1 second beacon rate and one with no neighbours, but the same data rates.

In the first sample the node was not transmitting and in the second the node was constantly relaying info between two nodes. In these samples no full beacon was scanned,so that term has been set to zero to get a more accurate read.
The first sample we calculate $I_{avg} = 225.757\mu{}A$ while the data shows $\bar{I} = 196.14\mu{}A$.
In the second sample we calculate $I_{avg,tx}(1) = 275.520\mu{}A$ while the data set shows $\bar{I} = 244.64\mu{}A$.
The last example with no neighbours we get $I_{avg} = 143.265\mu{}A$ and the data gives us $\bar{I} = 151.73\mu{}A$.

Now we can apply this model to the different configuration from \cref{tab:neo_configs}. This yields \cref{tab:nm_performance_overview} and in \cref{fig:nm_config_performance_overview} we can observe how the configurations perform given a percentage of the scheduled transmits sends data.

\begin{table}
    \small
    \centering
    \begin{tabular}{|c|c|}
        \hline
        \textbf{Config N}& \textbf{Average current consumption} \\ \hline
        % "worst" case
        1 & $234.79 \mu{}A$\\ \hline
        2 & $24.91 \mu{}A$\\ \hline
        3 & $20.39 \mu{}A$ \\ \hline
        4 & $17.41 \mu{}A$  \\ \hline
        5 & $12.89 \mu{}A$ \\ \hline
        6 & $228.64 \mu{}A$ \\ \hline
        7 & $12.69 \mu{}A$ \\ \hline
        8 & $31.77 \mu{}A$ \\ \hline
    \end{tabular}
    \label{tab:nm_performance_overview}
    \caption{Performance of different NeoMesh configurations}
\end{table}


\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/nm_config_performance_overview.png}
    \caption{Overview of NeoMesh performance given percentage of transmissions with payload}
    \label{fig:nm_config_performance_overview}
\end{figure}



\subsection{LTE-M}

In \cref{fig:lte_attach_sequence} we can se an LTE registration with the base station from a "cold" state, meaning there has been no previous communication between the LTE modem and the base station. It takes about 15 seconds, depending on the coverage in the area where the LTE modem is located.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/lte_attach_sequence.png}
    \caption{Current profile of intial LTE connection}
    \label{fig:lte_attach_sequence}
\end{figure}

The full first time connect can be divided into 4 stages, the first attach sequence, an RRC active phase, a DRX phase and then finally the sleep phase. They are all shown in \cref{fig:lte_attach_sequence}.

After the device has registered with the base station it requires less energy to maintain the connection as described in \cref{sec:theory}. In Trondheim, using a iBasis SIM card the nRF9160 was given a 1800 seconds Tracking Area Update timer and a 60 second active time. Which means that the base station expects the device to transmit data each 1800 seconds and the device must be reachable for 60 seconds after that.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/lte_coap_210_bytes_coap_transmission.png}
    \caption{A 210 bytes CoAP transmission from PSM}
    \label{fig:lte_coap_210b}
\end{figure}

In \cref{fig:lte_coap_210b,fig:lte_coap_21b} we can observe the current profile when the device wakes up from a PSM sleep interval to transmit data.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/lte_coap_21_bytes_transmission.png}
    \caption{A 21 bytes CoAP transmission from PSM}
    \label{fig:lte_coap_21b}
\end{figure}

In \cref{fig:lte_coap_210b} a transmission where the payload has a size of 210 bytes was done, which corresponds to transmitting 10 regular mesh payloads in one transmission, while in \cref{fig:lte_coap_21b} we transmit a single 21 byte payload.

\begin{table}[H]
    \centering
    \begin{tabular}{llll}
        \textbf{Event}      & \textbf{Active time} & \textbf{Mean current} & \textbf{Charge usage} \\\hline
        Initial connect     & $77.36 s$          & $21.622 mA$            & $590.690 mC$ \\

        Transmit 21 bytes   & $66.59 s$          & $2.1906 mA$            & $145.898 mC$ \\
        Transmit 210 bytes  & $66.45 s$          & $2.2175 mA$            & $147.378 mC$ \\
        Sleeping            & N/A                & $4.0814 uA $           & $t_{sleep} \cdot 4.0814\mu{}C$ \\
    \end{tabular}
    \caption{Overview of energy usage of LTE radio events}
    \label{tab:lte_energy_overview}
\end{table}

These events are summarized in \cref{tab:lte_energy_overview}. We can use this to calculate the mean current used to transmit the two payloads.

\begin{subequations}
    \begin{align}
        I_{mean} &= \frac{i_{sleep}\cdot t_{tau} + i_{tx}\cdot t_{active}}{t_{tau} + t_{active}}\\[1em]
        \label{eq:lte_mean_current_21b}
        I_{mean,21B} &= \frac{4.0815 \cdot 1800 + 2190.6 \cdot 60}{1860} = 74.6142\mu{}A\\[1em]
        \label{eq:lte_mean_current_210b}
        I_{mean,210B} &= \frac{4.0815 \cdot 1800 + 2217.5 \cdot 60}{1860} = 75.482\mu{}A
    \end{align}
\end{subequations}


\subsection{GPS}

To reduce the amount of work it takes to acquire GPS data an existing data set, from an internal GPS tracker project at Norbit, that contains data about the time spent to get a fix will be used in conjunction with a mean current usage when searching for GPS fix.
 
In \cref{fig:gps_time_spent_to_get_fix} we can see an overview over the time spent to get fixes in a deployed GPS tracker. In average the time spent to get a fix is 32.54 seconds.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/gps_time_spent_to_get_fix.png}
    \caption{Time spent to get fix}
    \label{fig:gps_time_spent_to_get_fix}
\end{figure}

The values in \cref{fig:gps_time_spent_to_get_fix} is extracted from a tracker that is attached to a car. The car is mostly driving around in Trondheim city, which means it moves further and less constantly around than sheep.

A current profile for the GPS search is also acquired, shown in \cref{fig:gps_search}, which lets us find the mean current consumption of a GPS search. From this data set of 30 seconds we find that the current usage have a mean of $35.194 mA$.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/gps_search.png}
    \caption{Current profile when searching for GPS fix}
    \label{fig:gps_search}
\end{figure}

\subsection{Thread}

For the Thread we have measured two different devices, one FTD or full thread device that acts as a leader in the network and one minimal thread device that is a mesh node and runs on ultra low power.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/ot_fed_current_usage.png}
    \caption{Full Thread Device current profile}
    \label{fig:ot_ftd_profile}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/ot_mtd_current_overview.png}
    \caption{Minimal Thread Device current profile}
    \label{fig:ot_mtd_profile}
\end{figure}

\subsection{Error sources}

In ultra low power designs(ULP) it is important to choose a highly efficient PMIC with a very low quiescent current. A ULP device will spend most of its time in a sleep or deep sleep state which makes the quiescent current that leaks through the PMIC non negligable in proper designs.
In the experiments performed in this paper we ignore the losses of the PMIC. All Nordic development kits are developed with current measurement capabilites in mind while the NeoMesh module is a simple breakout board which means it should work very closely to a production setting.

A good few months into the measurement work a flaw was found in the N6705B. When measuring the fast dynamics of the current profiles the instrument could not switch between current ranges quick enough. This means that most of the readings done by the N6705B have an unknown bias and low accuracy during sleep, i.e. currents in the $\mu{}A$ ranges. Some of these measurements are still used in the analysis, but to reduce the impact of the inaccurate low current measurements only the section with radio activity is used and new samples of sleep events where done with the PPK2.

The NeoMesh average current model is only based on empirical data, so it may not be used as an exact model. To create a more accurate model better instruments should be used to capture more of the dynamics in the model.
A assumption that is made is also that the different radio events are perfectly periodic, which is not the case. When autocorrelating the datasets we find that the events are a bit off.
There might be a technical reason for the period offset, but no references to this was found in the documentation \cite{neocortec_nc2400},\cite{neocortec_user_guide},\cite{neocortec_integration_manual}.

