\section{Theory}\label{sec:theory}

To make sure that the reader have sufficient knowledge of the terms and technologies described in this paper this section will explain terms and technologies that are central to the paper.

\subsection{Mesh networks}

A mesh network is a network topology where every node in the network \textit{may} connect to any other node. There are two types of mesh topologies, the partial mesh- and the full mesh topology.
In the full mesh topology all nodes can communicate directly to any other node in the network. This is illustrated in \cref{fig:full_mesh_illustration}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/4_node_full_mesh.png}
    \caption{Illustration of a full mesh topology}
    \label{fig:full_mesh_illustration}
\end{figure}

The more common of the two is the partial mesh topology which is similar to the full mesh topology, but the nodes are not fully connected with every other node. In this topology every node is still reachable by every other node, but packages sent between them might need to be relayed by one or more intermediary nodes. This is illustrated in \cref{fig:partial_mesh_illustration}
When a package is relayed by an intermediary node it is often called a \textit{hop}. So direct communication is one hop, while each intermediary node will add one hop to the packet path.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/large_non_full_mesh.png}
    \caption{Illustration of a partial mesh topology}
    \label{fig:partial_mesh_illustration}
\end{figure}

In a mesh network there may be many paths from one specific node to another. This in turn means that many nodes may be disconnected before a path between two nodes disappears. This makes mesh network resilient to broken paths and nodes disconnecting from the network like shown in \cref{fig:mesh_recilence_illustration}.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/full_mesh_many_paths.png}
    \caption{Illustration of a resilient mesh topology with many paths}
    \label{fig:mesh_recilence_illustration}
\end{figure}

\subsection{NeoMesh}

NeoMesh is a protocol that will be used a lot in this thesis, as stated in \cref{sec:research}, it is a hierarchy free mesh network \cite{neocortec_user_guide}.

The working principle of the NeoMesh protocol is to use time synchronization to allow nodes in the network to sleep most of the time.
To make sure all nodes are synchronized a requirement is that every node in the network have the same configuration.
The configuration includes the rate of beacon and transmissions in the network, which is needed for every node to calculate how often it should sleep.

A beacon transmission is sent at an interval between 1 and 30 seconds. When a node is powered on and not included in any network it will start to transmit beacons and scanning for beacons from other nodes.
After powering up a node will perform a series of full beacon scan.
When a full beacon scan is performed, the node will keep its radio on for the entire period of the beacon transmission interval.
This means that any beacons transmitted within range should be received.
When a beacon is received the node will synchronize itself with the node that transmitted the beacon and communication can start. These beacons and a full scan is shown in \cref{fig:nm_full_beacon_scan_illustration}, here the two nodes are synchronized after the full beacon scan.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/neomesh_full_beacon_scan.png}
    \caption{Illustration of beacon scanning in NeoMesh, from \cite{neocortec_user_guide}}
    \label{fig:nm_full_beacon_scan_illustration}
\end{figure}


Within the beacon transmission each node adds information about the next time it plans on transmitting data.
So when a node receives a beacon it will start a timer so that it is awoken during the other nodes next transmission.
It is illustrated in \cref{fig:nm_tx_synch}, after the second beacon is transmitted the two nodes are synchronized and can transmit data between each other.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/neomesh_transmission_synch.png}
    \caption{Transmission synchronization in NeoMesh, from \cite{neocortec_user_guide}}
    \label{fig:nm_tx_synch}
\end{figure}

When nodes are synchronized to each others scheduled transmits they are considered neighbours.
Now we may transmit packages across the network. As stated in \cite{neocortec_integration_manual} the maximum payload of a package is 21 bytes, which will be used later as a baseline for data transmissions.
When a node transmits a package to a different node it just transmits the package during its scheduled transmit, knowing that its neighbours will be listening.
When a packet is transmitted to a node that is note ones immediate neighbour, it is routed through the network based on a routing table that each node holds.
An illustration of how packets travel in the network is shown in \cref{fig:nm_packet_travel}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/neomesh_packet_traveling.png}
    \caption{Packet flow in a NeoMesh network, from \cite{neocortec_user_guide}}
    \label{fig:nm_packet_travel}
\end{figure}

Network maintenance in NeoMesh is baked into the scheduled transmission events. Even though beacon transmissions is necessary for normal operation, after the network have been formed, it is still transmitted.
This is to handle the cases where nodes move around and the topology changes. The beacons will then be used to update neighbours throughout the networks lifetime.

If a node does not receive any data from its neighbour on, a configurable, number of scheduled transmissions the neighbour is considered lost.

NeoMesh exposes an application layer to the user which will be used to control the nodes.
The application layer supports different message types, different means to control nodes and other useful features.
In this thesis the acknowledged packet type will be used for transmitting data.

\subsection{Quick comparison of LTE-M, LoRa and NB-IoT}

Both LTE-M and NB-IoT are specified by 3rd Generation Partnership Project(3GPP). LTE-M was first introduced in 3GPP Release 12 and NB-IoT in Release 13.
In this paper LTE will be used as a group term for both technologies, even though LTE consists of much more then LTE-M and NB-IoT.
LTE is useful because it uses existing telecommunication infrastructure, more specifically the 4G infrastructure \cite{telenor_iot_difference_guide}.

The LTE-M standard is optimized for larger data rates and is suitable for non-stationary nodes. This includes f.ex. transportation or supply chain tracking \cite{telenor_iot_difference_guide}.
The NB-IoT technology is a single 200 kHz narrow band(NB) network, it is more suitable for stationary nodes that have long battery life and low cost.

LoRa, short for Long Range, is a low-power, long range wide-area network. LoRa offers even lower data rates and due to no collision detection it is best suited for low cost, low-power, and low density applications.
LoRa uses its own proprietary gateway system which runs LoRaWAN (Long Range Wide Area Network). Gateways can be purchased and set out by consumers or companies, so the network coverage has quite good \cite{lora_coverage}.

So in summary, LTE-M supports the highest data rates, NB-IoT is in the middle and LoRa supports the lowest data rates. LTE-M also has support for moving nodes, while NB-IoT is optimized for stationary nodes, no literature have been found to indicate that LoRa does not support moving nodes.

\subsection{LTE power saving}

To be able to have meaningful discussions about the use of LTE in IoT a proper introduction to power saving features of the standard is required.
When a device with an LTE modem wants to reach the internet it first needs to \textit{attach} to the network. 
This attachment procedure is a set of complex steps, illustrated in \cref{fig:lte_attach_process}, involving many systems and is out of scope for this paper.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/lte_attach_illustration.png}
    \caption{LTE Attach Sequence, User Equipment(UE) is the LTE modem}
    \label{fig:lte_attach_process}
\end{figure}

It is sufficient to say that the process requires a lot of transmitting back and fouth between the modem and the LTE base station.
When the device is attached it should have received an IP address and may reach the internet over LTE. While attached to the LTE network and to be able to receive data the modem must keep the radio powered on at regular \textit{paging intervals}\cite{lte_startup_guide}. These paging intervals are used be the base station to send information about data for the device. In addition to this periodic \textit{Tracking Area Update}(TAU) messages must be sent to the base station to stay attached.

In a ULP setting sending these messages and keeping the modem radio on during paging intervals may consume and order of magnitude more current on average than what is acceptable.

To be able to use these LTE technologies in IoT we need to remove or sufficiently reduce these operations to achieve adequate power consumption. This can be achieved by using one of the different power saving features that LTE-M and NB-IoT supports.
Fist we have Power Saving Mode(PSM), which negotiates an increased pTAU interval and an \textit{active time} with the base station. The pTAU interal can be everything from a few minutes to a few hours which greatly reduces average power usage. The active time is a period after the device has sent a pTAU where it must be ready to receive paging instances so it is reachable by the base station, the active period can be from a few seconds to a few minutes.
An important point here is that it is the base station that decides which pTAU and active time the device gets. A device may \textit{request} a pTAU and active time, but the base station might not grant it.

Another power saving feature is the Extended Discontinous Reception(eDRX) which is an extention of the Discontinous Reception(DRX) used by many smartphones today to save battery power. eDRX may be used in conjuction with PSM to achieve greater power saving.
In short it greatly increases the interval where the device is not listening to the network allowing the modem to switch of and save power. Like PSM the interval of eDRX must be negotiated with the base station and the base station decide wether the interval is granted or not.


\subsection{Satellite IoT}

There are some companies that have utilized satellite to communicate with the cloud. This possibility was discovered too late to be considered in this paper, but it might be a possible replacement for LTE.
Satellite IoT comes with a few benefits. It will have a much larger coverage as it is not dependent on any base station on earth. It is also reliant as it often builds on a very fault tolerant L1-Band satellite service \cite{satellite_iot}, \cite{l1_band}.
Satellite IoT will work in mostly the same way as LTE, you acquire a SIM card for authentication and a modem that supports communication with the satellites and off you go.

\subsection{Global Navigation Satellite Systems}

Global Navigation Satellite Systems or more often GNSS is a system that is used in many different contexts to acquire position and velocity data for a piece of equipment. A quick intro to GNSS is required to discuss the power usage of the GNSS in \cref{sec:impl}, but it will be kept brief as it will quickly fall out of scope for this paper.

GNSS systems are implemented by using satellites. A set of 20 to 30 satellites are in orbit for each system which broadcasts periodic signals.
The signals themselves contains orbital information about the satellites, as well as data about the satellites on board atomic clock \cite{gps_positioning}.
When a receiver can observe the signals sent from a satellite it is said that the satellite is \textit{in view}.
A receiver will have to listen to these signals, extract the orbital and clock correction data, and then use this data to estimate the distance to each satellite and any clock drift (caused by the receivers comparably terrible clock).
When the clock error and relative distance to each satellite is acquired, the receiver can calculate its position on the earth, by using the satellites orbital data \cite{gps_positioning}.
This method is based on the relative distance to each satellite in view and the clock error, this means that a minimum of four satellites are required to be in view for the entire duration of the estimation process.
In short, to get a GPS fix we require four satellite in view.

The clock correction and orbital data transmitted by the satellites are transmitted at a rate of 50bps.
An entire navigation message (almanac, ephemeris, clock data and some control data) consists of 25 frames of 1500 bits.
This means that downloading all the data takes 12.5 minutes.
It is also required that the entire package is downloaded at once, so if a device loses satellite coverage it must start over.
The almanac, containing coarse orbital parameters, is valid for 6 days, the ephemeris, precise orbital parameters, is valid for 2 hours and the clock data needs to be continuously up to date(but is also transmitted more often).

Having no valid orbital or clock data and having to collect all this data before getting a fixed is often referred to as a \textit{cold} start.
If orbital data is valid, but our initial position guess is no longer we refer to this as a \textit{warm} start.
When orbital data, clock correction and our initial guess is valid we call this a \textit{hot} start. Which often takes just a few seconds.

Another important factor in this process is that GNSS estimation requires an initial "guess" at the receivers position. 
This is often solved by placing the initial guess at the center of the earth, but this will lead to a longer time spent to get a fix.

To reduce the time to first fix(TTFF) a support system called Assisted GNSS (A-GNSS or A-GPS where GPS is used) is implemented.
This system allows a GPS receiver, often one that can communicate with GPRS base stations, to download navigational messages and initial position guesses from the internet.
The data is based on the GPRS mast it is communicating with. The mast have a known location and can continuously hold updated navigational messages.
Using A-GPS will let our equipment go straight to a warm or hot start without having to download any navigational data.

