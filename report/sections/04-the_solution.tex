% Here we describe the things we want to test on a higher level.
% Test specification
\section{Proposed design}\label{sec:design}

In this section we will explore an imagined sheep tracker's components and functionality.

The main components of such a tracker would be a microcontroller, a GNSS module, a gateway module like 4G or LoRa, a mesh module, a battery with a power management IC(PMIC) and some sort of mounting solution.
A component diagram showing this proposed design is shown in \cref{fig:sheep_tracker_comp}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/sheep_tracker.png}
    \caption{Proposed sheep tracker components}
    \label{fig:sheep_tracker_comp}
\end{figure}

In most cases an MCU would also include a gateway module or mesh module, but for the sake of generality these are two separate modules in the design.

This proposed design of a tracker is not very specific. With enough time and engineering effort it is possible to build a tracker by the modules that we have presented in this section.
While it would be interesting to see how the tests would run on an actual implemented tracker, this is out of scope for this thesis.

The microcontroller will be responsible for coordination of each of the peripheral units. Its job is to communicate with the cloud, via the gateway module, extract GNSS data from the GNSS module, coordinate sleep and communicate with the other mesh nodes. The complete software solution will not be discussed in more detail in this thesis.

The GNSS module will require an antenna that can receive the GNSS signals it should support sleeping and/or a configurable duty cycle to save power.

The gateway module will be responsible for handling connections and data transmissions to the cloud. It should controllable by the MCU.

The mesh module will be responsible for maintaining the mesh network and to transmit data to other nodes in the network.

The power management system should be designed with ultra low power in mind with a support for the voltage range of the batteries used.

\section{Test specification}\label{sec:test_spec}

In this section a set of test scenarios will be described that will be used to investigate the comparative power usage of mesh protocols and network communication.
First a presentation of the test setup of the gateway technology will be presented, then the mesh network setup.

Before we go into the details of how we want to test the two different communication methods it is important to discuss their differences.

\subsection{Gateway energy usage}

To have sufficient data to compare mesh and gateway energy consumption we need a few important metrics.
Network maintenance will be measured, it will include the cost of connecting to the network and cloud, but also how much energy it takes to keep that connection alive.

Next a measurement of the cost of sending data to the endpoint will be required. To measure this we will look at the total amount of data sent compared to how much energy is spent. This is done to account for the differences in application layer protocols that can be used for the gateway protocol. Then we can analyse the findings and see how well we can amortize the cost of sending data over the cost of network maintenance.

Then we will combine these two measurements into an average energy usage. This will be done by averaging the cost of data transmissions over the time where the module is idle. This should give us an average cost of using the gateway, given the frequency and size of payloads transmitted.

The practical test setup will be simple. Program some sort of gateway unit with a program that sends a known sized package to the endpoint.
Then identify the network maintenance part and the data transmission part of the measurement.
When the two parts of the transmission are identified one can calculate the energy usage per byte from the transmission part of the data.

\subsection{Measuring IoT mesh network energy usage}

When measuring the power consumption spent on mesh communication there are a few parameters that needs to be kept in mind.

The topology of the mesh network will affect the energy performance of the network. 
For example a full mesh topology may require more energy to maintain the network than a line topology, where all nodes only have two neighbours, or vice versa. This might be protocol dependent.
To make sure that we account for this measurements in a small variety of topologies should be done. Interesting topologies might include the full mesh, where every node can communicate in one hop. The single line topology, where all the nodes are in a single line, meaning that end-to-end communication will reach every single node in the network. The star topology where a single node will be the choke point of every message in the entire network.
Which topologies that should be measured will depend on the protocol used and how that protocol handles network maintenance and routing.

The main ways that the mesh network will spend its energy is by a) sending messages, b) receiving messages, c) relaying messages and d) doing network maintenance.

To be able to create a sufficiently detailed model of the energy usage all four of these points should be analysed. The reason that relaying messages has its own point is that, depending on the protocol, relaying messages might require more or less computations energy than just sending and receiving a message. The network maintenance will also be heavily dependent on the protocol that is used as most protocols use their own routing algorithms.

\subsection{GNSS positioning system}

GNSS is a complex system and have many complex external parameters that decide its performance. To get ideal data about GNSS performance testing the receiver in as realistic conditions as possible is important.
Since it is not always feasible to deploy a set of realistic test a heuristic approach could be taken. Try to find or create a data set that is as representable as possible and use that as an estimate.
Since we have a very clearly defined scenario to work against we can use this to decide how such a data set should look.

We need a dataset where the GNSS coverage might be varying quite a bit. Sheep may graze in an open field or in a dense forest. This will have a large impact on GNSS fix times as dense foliage will block GNSS signals.
We also need some data on how much current is spent when the GNSS is actively searching for a fix. This should be as simple as starting a GNSS search on the GNSS module and measuring the current usage over a time interval. Since the GNSS current usage is not very dynamic this should be a decent estimate.

From these data sets a mean time spent to get fixes can be used to then calculate the average energy consumption on each fix.

