\section{Discussion}\label{sec:discussion}

In this section we will discuss the results presented in the last section.

\subsection{Viability of Thread}

First we will discuss the viability of the Thread protocol in the context of homogeneous mesh networks.
Since the network is assumed to be homogeneous it means that all nodes will have access to the same amount of energy after each recharge.
From the analysis of the OpenThread devices, more specifically the Full Thread Device in \cref{fig:ot_ftd_profile}, we can see that the
current consumption of such a device is at $\tilde 12mA$. This of course comes from the fact that a FTD is required to keep its radio on at all times. Radio is one of the most expensive activities our nodes can perform, so this is not ideal.

While the Minimal Thread Device supports a very small sleep current, down in the nano amperes, the $12 mA$ mean current consumption of the FTD makes the Thread protocol unfit for this specific use case. Thread FTDs should have a larger source of power than a small battery.

\subsection{Energy usage in NeoMesh}

The main parameter that decides the performance of the NeoMesh network in terms of energy usage is also the parameter that decides the networks performance in terms of network throughput and how long it takes networks to form and change.
That means that when deploying a NeoMesh network it will in large to figure out the tradeoff between network performance and energy performance.
For some networks a high throughput may be desired, but this will result in lower battery life and vice versa.

For a sheep tracker we can make a few assumptions about how the nodes will move and change over time. As mentioned earlier sheep tend to travel in sets, one sheep and two lambs. This means that a network of three nodes is very common and they will most likely stay within range of each other most of the time.
Since sheep are not the quickest of creatures a fairly slow scheduled transmit period could be chosen and an even slower beacon rate. This means that something like configuration 8 from \cref{tab:neo_configs} may be quite ideal for this sort of usage.

One thing this thesis have not touched is the network performance in such a mesh network. It may be a problem that is worth further studies.
The proposed model in this paper is also built on a limited set of observation, so a more detailed model may be worth pursuing.

\subsection{LTE-M performance}

It is quite clear from the results that LTE-M is not ideal to communicate with the cloud at a high rate. Transmitting data over LTE-M is quite expensive, but in turn it is also quite efficient.
The difference between sending 21 and 210 bytes to the cloud is almost negligible which makes it quite good for transmitting larger amounts of data to the cloud at slower intervals.

As of writing this thesis the LTE-M performance is also very much based on the PSM parameters that are granted to the receiver from the LTE network. This will likely improve in the future when IoT infrastructure is improved and more specific PSM parameters can be requested for different use cases.

\subsection{Cost of Positioning}

Acquiring GPS fixes are quite expensive in terms of energy, which is clear from the $34.04 mA$ mean current usage and the average 30 seconds spent to get GPS fixes.
If every sheep reports their position, lets say once and hour this adds another $34.04 \cdot 30 / 3600 = 283.36 \mu{}A$ of average current usage for each node.
Given that even the most energy hungry NeoMesh configuration only draws $234 \mu{}A$ on average, this is a large cost.

An interesting solution to this problem may be to only acquire a position fix on one node in the mesh network and then have that node distribute the position fix to its neighbours.
Since mesh networks have quite limited range and GPS positioning may have a considerable error using the GPS fix across many neighbouring nodes may be accurate enough for many systems.

In sheep tracking we have the additional information that a lamb will follow its mother during the entire grazing season. 
This means that, as long as no predator scares the set and drives them apart, to find a set of sheep you can use any of their positions. Now assume that we are looking at a single set, i.e. a sheep and its lambs, if they are not scattered by predators the cost of GPS positioning will be amortized over the three nodes over time.
Leaving us with a cost of $234 \mu{}A / 3 = 79 \mu{}A$ which is a large energy saving.

Also, since our nodes are homogeneous, if the set is indeed scattered by a predator a farmer may be notified(by the sets mesh network being split up) and they may travel out and rescue the separated lambs. Normally if a sheep is killed by a predator the lambs are considered lost because they cannot survive on their own.
The tracked lambs may be rescued by releasing a sheep with no lambs close to the lost lambs and they will follow the new mother for the rest of the season.

As discussed in \cref{sec:theory} to reduce the time to get a first fix we can use a A-GNSS system. Now since all the nodes very likely is using the same base station for LTE communication, they will all get the same A-GNSS data from the mobile network. 
So another optimization may be to have a single node fetch the A-GNSS data and then broadcast it to other nodes using the mesh network.

It would be interesting to conduct further studies on if, and potential how, one can optimize GPS positioning in a mesh network like the one proposed in this thesis.
Since we have a lot of knowledge about the positioning of nodes, relative to each other it should be possible to f.ex. gather more than one GPS fix in the network and then use these data points and the network topology to further increase accuracy of the position of each node.
This might be a computationally heavy task so it might be more suited for being solved in the cloud.

\subsection{Mesh communication vs. LTE-M}

In the sheep tracker setup the NeoMesh and LTE-M serves two different purposes. Mesh is used for local, node-to-node, communication while the LTE-M component is used to report state and update parameters of the nodes.
Now the big question is if sharing state via mesh and then doing a shared LTE-M transmission is more efficient than every node transmitting its own state via LTE-M.

In our scenario we have three trackers, one set of sheep, that grazes close together and their trackers have formed a mesh network.
Given that we utilize configuration 8, from \cref{tab:neo_configs}, which would give us a nice trade-off between network latency and power consumption. Also lets assume that the sheep is configured in a full mesh topology most of the time. 
How will the energy usage compare to the scenario where each tracker gets a GPS fix by itself and then reports it at 1800 second intervals?

In the first case we assume that we search for a GPS fix at each LTE-M active time. In this case the energy usage will be the mean current of the nRF9160 while transmitting 21 bytes of payload(in this case, 4 bytes longitude, 4 bytes latitude, 4 bytes altitude with a few bytes for other control data/state) including the current usage of a GPS search each half hour.

This yields us a mean current of:
\begin{equation*}
    74.6142 \mu{}A + 35.194\cdot 10^{3}\mu{}A \cdot \frac{32.54}{1800} = 656.587\mu{}A
\end{equation*}

In the second case we have the nodes connected through mesh. They share state continuously and only a single node transmits LTE and a single node searches for GPS.
This means that our mean current will be the sum of the average current of NeoMesh configuration 8, the LTE transmission and GPS searches, but the two latter are divided by 3.
Since we are sharing state we cannot ignore the fact that the nodes will be transmitting via mesh. They will be doing this constantly, regardless of LTE transmissions or GPS search, so to keep state updated on a 30 minute bases about 40\% of scheduled transmits having payloads should be more than enough.

All of this gives us

\begin{equation*}
    35.753 \mu{}A + \frac{74.6142 \mu{}A + 35.194\cdot 10^{3}\mu{}A \cdot \frac{32.54}{1800}}{3} = 254.615\mu{}A
\end{equation*}

Now these results are heavily reliant on the fact that it is possible to more or less trivially share state between nodes. This might not be the case and the synchronization of transmissions, GPS searches and general state is a possibility of further studies.
There are also quite a few assumptions made about the signal levels and LTE coverage in this thesis. It would be interesting to do a more specific study on how different topologies would affect this sort of solution and how well it would perform on actual sheep.

