% Here we describe the actualt test software, test setups and test circuits.
\section{Test setup, equipment and instruments}\label{sec:impl}

In this section we will discuss the implementation of each module of the sheep tracker, how the software was designed, and present the methods use to test each module.

\subsection{Instruments}

A Keysight N6705B DC Power Analyzer will be used to measure the current and voltage output in some experiments. This instrument features up to 24.80 micro seconds sampling rate and 0.025\% + 8 nA accuracy.
To make measurements the device under test will be connected to a channel on the instrument and an output voltage will be configured.
When a measurement is started the instrument will sample the current output over time and the data can be extracted from the instrument as a CSV file.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/n6805b_overview.jpg}
    \caption{Overview of N6705B power analyser display}
    \label{fig:n6705b}
\end{figure}

The N6705B is a great instrument to measure the active current profile, but with the IoT nodes larges jumps in current it does not have a large enough dynamic range to be able to measure sleep currents accurately enough.
To supplement the measurements the Nordic Semiconductor Power Profiler Kit(PPK) 2, shown in \cref{fig:ppk2_image}, will be used to do additional measurements on sleep currents. The PPK features accuracy all the way down to 200 nA and a large dynamic range. This makes it possible to measure accurate current profiles of IoT devices.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth,angle=90]{images/ppk2_image.png}
    \caption{Image of the Nordic Semiconductor Power Profiler Kit 2}
    \label{fig:ppk2_image}
\end{figure}


The PPK supports two modes, an ammeter mode and a supplied meter mode. In ammeter mode it simply samples the current flowing through the kit, while in supplied meter mode the PPK supplies a variable current between 5 and 1.8 volts and measures the current draw.
A sampling rate of 100kHz gives us a very nice view of the radio events that we measure.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/ppk_test_setup.jpg}
    \caption{PPK test setup}
    \label{fig:ppk_test_setup}
\end{figure}

As shown in \cref{fig:ppk_test_setup} we connect the DUT with twisted power wires and our desired "marker" signals are connected to the IO header.

After a measurement is completed the data can be viewed and exported from a graphical interface. Shown in \cref{fig:power_profiler_app}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/power_profiler_app.png}
    \caption{Screenshot of the Power Profiler application}
    \label{fig:power_profiler_app}
\end{figure}



\subsection{NeoCortec NC2400}

For the mesh protocol to compare against LTE-M we will utilize the NeoCortex NC2400 module\cite{neocortec_nc2400}. The NC2400 provides a fully functional mesh network stack and can easily be configured using the included configuration software.
The module provides a mesh network called NeMesh, explained in \cref{sec:theory}. NeoMesh is suited for this thesis because it does not rely on a coordinator node to create it's network and is developed with ultra low power in mind.
These two features will make it an ideal mesh network to test our hypothesis as all nodes should be homogeneous. In this paper the FWNC2400C will be used as it is a simple breakout board for the NC-2400 module.
The module uses a 2.4GHz radio and comes with the \textit{NeoMesh Protocol} stack. The included NeoMesh Protocol is a mesh protocol designed for flexibility and reliability.

A module is set up by simply soldering on a RS232 header and a set of power pins. Then we connect 3.3V and we configure it using the NeoCortec Configuration tool.
To make sure we have reliable data and make sure the module is as isolated as possible a few key configurations are done.

Since the NeoMesh performance is very dependent on the configuration of timing parameters like the scheduled transmit rate and beacon rate we want a set of configurations that we can use in evaluation. In \cref{tab:neo_configs} a set of configurations are presented that will give a broad overview of the performance of NeoMesh in different configurations.

\begin{table}
    \small
    \centering
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        \textbf{Config N}& \textbf{TX Power} & \textbf{Scheduled Data Rate} & \textbf{Beacon Rate} & \textbf{Beacon Full Scan Rate} \\ \hline
        % "worst" case
        1 & 1dBm & 1s & 1s & 500 \\ \hline
        2 & 1dBm & 15s & 15s & 500 \\ \hline
        3 & 1dBm & 15s & 15s & 1000 \\ \hline
        4 & 1dBm & 30s & 30s & 500 \\ \hline
        5 & 1dBm & 30s & 30s & 1000 \\ \hline
        6 & -30dBm & 1s & 1s & 500 \\ \hline
        % lowest energy usage
        7 & -30dBm & 30s & 30s & 1000 \\ \hline
        8 & 1dBm & 5s & 30s & 1000 \\ \hline

    \end{tabular}
    \label{tab:neo_configs}
    \caption{Different configurations used when evaluating NeoMesh}
\end{table}

To be able to test out different mesh topologies the modules were placed with sufficient distance between them so that the required topology was formed.
The node under test was connected to the power analyser and the other nodes were moved or had their antenna attenuated to form the required topologies.
A node test "station" was made as shown in \cref{fig:neomesh_node_station} and placed around the office space.
\cref{fig:neomesh_node_attenuation} shows an attempt to attenuate the signal to reduce the radiated signal, reducing the signals power, but not affecting the node's transmit power. 
This method proved unsuccessful and the transmit power of nodes that are \emph{not} under test will be reduced in stead.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/neomesh_node_station.jpg}
    \caption{A NeoMesh node "station"}
    \label{fig:neomesh_node_station}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/neomesh_node_station_attenuated.jpg}
    \caption{A NeoMesh node with makeshift attenuation}
    \label{fig:neomesh_node_attenuation}
\end{figure}




\subsection{Thread}

We will also analyse the Thread protocol. To set up a simple thread network a few nRF52840 development kits will be used. The nRF52840 is built for BLE, but also supports OpenThread.
The code for the project will be a modified example from the nRF Connect SDK. This will mostly be an attempt to see if it is at all possible to use a IEEE 802.15.4 based protocol for this purpose.

In the nRF Connect SDK a OpenThread sample can be found that implements a CoAP server \cite{nordic_ot_server} and client \cite{nordic_ot_client} over OpenThread. This will be the example used for testing the OpenThread network.
The CoAP server exposes two endpoints, one "provision" endpoint for pairing a client with a server, and one "light" endpoint which is used to set the state of an LED on the server node. The server will always be configured as a Full Thread Device, mostly because the sample code is not configure to also work as a Sleepy End Device, but it should be possible to implement the server as a SED as well.
The CoAP client is configured to include Minimal Thread Device capabilities. The client can therefore be a SED in the Thread network. The client sends a provision request to a server to pair with it, then it can send "/light" requests to the server to toggle LEDs.

Since the sample is meant to be used as an example on how to start development using the OpenThread stack on the nRF52, some modifications was made to achieve comparable deep sleep power usage.
On both the server and the client node the shell and UART was disabled, this peripheral draws large amounts of current and should not be active at all times. The system power management and tick-less idle options from the Zephyr kernel was also enabled to further reduce the power usage.
Then the development kits were flashed with software. 
When testing the kits the nRF52 was disconnected from the interface MCU that is used to flash the kits, and the N6705B was used as an external power supply to measure both voltage and current usage.
This resulted in 5 \mu{}A of average sleep current on the SED. It should be possible to optimize the nRF52 for even less power usage, but this would be sufficient as a baseline for these tests.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/thread_test_setup.jpg}
    \caption{The test setup when testing Thread}
    \label{fig:thread_test_setup}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/thread_test_connections.jpg}
    \caption{The two development kits used when testing Thread}
    \label{fig:thread_test_connections}
\end{figure}

\subsection{Gateway - 4G}

The nRF9160 is another SiP that provide an application processor with an integrated LTE-M/NB-IoT modem and a GPS receiver. 
The nRF9160 development kit will be used for testing 4G connectivity and GPS in this paper.
To be able to connect to the 4G network we need a SIM card, for this paper a SIM card from 1NCE is used. It may roam in Norway and it supports PSM mode.

A basic CoAP client sample(from the nRF Connect SDK) \cite{nordic_lte_coap_example} was used as a starting point for our measurements. It is a simple example that turns on the modem, connects to LTE-M and then requests a timestamp from a CoAP server.
To get a broad set of tests a lot of changes was made to test different uses of the 4G modem. Firstly the UART peripheral was disabled to save power and the system power management and tickless idle features where enabled in Zephyr.
This brought the average sleep current down to about 5 \mu{}A which is a sufficient baseline, though further optimizations could be done here as well.
For the first test a CoAP transmission was done every 20 seconds, leaving the modem on between each transmission while the CPU was sleeping. In the next test the modem was disabled and enabled between each transmission and in the last test PSM was enabled. PSM enables the modem and the base station to agree on an active and inactive period which the station should be ready for packets from the modem.

When the modem is turned off and on between transmissions the modem needs to connect to the radio tower for each cycle.
When PSM is enabled the modem does not have to constantly reconnect to the tower, but may still almost completely turn off.
This saves a lot of power and is the main feature that makes 4G LTE-M viable for IoT, considering it allows for a floor current of 4 \mu{}A during PSM \cite{nordic_online_power_profiler}.

For reference measurements when PSM mode is enabled were acquired.
Currently, in Norway, using the 1NCE SIM card the nRF9160 was handed out a 1800 seconds pTAU interval and a 60 seconds active time.
This means that for all our measurements there will be a 60 second interval where the modem turns on its receiver for paging indicators.
The PPK kit was used to measure this current as the N6705B only allows for a total of 30 seconds of sampling, with a sufficient sample interval.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{images/lte_test_connections.jpg}
    \caption{nRF9160 development kit used for testing LTE}
    \label{fig:lte_test_connections}
\end{figure}

As shown in \cref{fig:lte_test_connections} the development kit is connected to an external power supply to reduce noise. In this example we use the Agilent E3640A DC Power Supply. The power supply is connected to the P28 header. The power analyzer is set to current measurement mode and connected to the P24 header which is used to measure the current of the chip. This setup was used to measure the active times of the LTE modem.

\subsection{GPS}

The nRF9160 DevKit also includes a GPS receiver and antenna. This was used to test out the GPS energy usage.
The goal is to measure current draw while searching for a GPS fix. The test setup is the same as in \cref{fig:lte_test_connections}.

Most of the software from the LTE testing was reused. This software was already configured for optimal sleep currents.
The changes that were made was that in stead of connecting to the LTE network the nRF9160 started a GPS search in stead.
Then a measurement was done to acquire the average current drawn when searching for a GPS fix.

A study from the UK \cite{gps_tracking_sheep} who conducted a GPS tracking study on sheep had a GPS fix success rate at above 95\% on a 30 minute interval.
With a properly configured GPS receiver it is assumed that a success rate close to this should be achievable.
Based on this an assumption that each tracker manages to get a successful fix each time it searches is made.

